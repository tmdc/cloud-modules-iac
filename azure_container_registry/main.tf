# TODO: Verify if Standard is acceptable, and if not move to Premium and Add Network Rules to Limit access to K8S
resource "azurerm_container_registry" "main" {
  name                     = "${var.prefix}0acr0${var.environment}"
  location                 = var.location
  resource_group_name      = var.resource_group_name
  sku                      = "Standard"
  admin_enabled            = true
  tags = var.tags
}
