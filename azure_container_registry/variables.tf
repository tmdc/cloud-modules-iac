variable "prefix" {
  description = "The prefix for the resources created in the specified Azure Resource Group"
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "location" {
  default     = "westus2"
  description = "The location for the resource"
}

variable "resource_group_name" {
  description = "The name of the resource group that resource should be created in"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}