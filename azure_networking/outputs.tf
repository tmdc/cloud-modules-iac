output "azurerm_virtual_network" {
  value = azurerm_virtual_network.cloud_kernel_vnet
}

output "azurerm_virtual_network_id" {
  value = azurerm_virtual_network.cloud_kernel_vnet.id
}

output "ck_public_subnet_id" {
  value = azurerm_subnet.public.id
}

output "ck_private_subnet_id" {
  value = azurerm_subnet.private.id
}

output "ck_public_subnet_name" {
  value = azurerm_subnet.public.name
}

output "ck_private_subnet_name" {
  value = azurerm_subnet.private.name
}

output "sg_public" {
  value = azurerm_network_security_group.public
}

output "sg_private" {
  value = azurerm_network_security_group.private
}

output "nat_public_ip" {
  value = azurerm_public_ip.natpublic_ip.ip_address
}