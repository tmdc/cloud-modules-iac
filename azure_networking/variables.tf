variable "resource_group_name" {
  description = "The name of the resource group that AKS should be created in"
}

variable "location" {
  description = "The location for the AKS deployment"
}

variable "short_prefix" {
  description = "The short prefix for resources that have name length limitations and delimiter limitations"
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}

variable "cidr_block" {
  description = "The ip block to associate with the vnet"
}

variable "private_subnets" {

}

variable "public_subnets" {

}

variable "pet_name" {
  description = "petname"
  default     = "owl"
}