
module storage_kafka {
  create_storage      = var.create_kafka
  source              = "git::https://bitbucket.org/tmdc/cloud-modules-iac.git//azure_storage"
  resource_group_name = var.resource_group_name
  location            = var.location
  subnet_ids_to_allow = [var.subnet_id]
  short_prefix        = var.short_prefix
  short_postfix       = "k"
  environment         = var.environment
  account_tier        = "Standard"
  account_replication_type = "ZRS"
  is_hns_enabled      = false
  tags = var.tags
}

resource "azurerm_storage_container" "kafka_main" {
  count                 = var.create_kafka ? 1 : 0
  name                  = "${var.prefix}-kafka-hdi-${var.environment}"
  storage_account_name  = module.storage_kafka[0].storage_account_name
  container_access_type = "private"
}

resource "azurerm_hdinsight_kafka_cluster" "main" {
  count               = var.create_kafka ? 1 : 0
  name                = "${var.prefix}-kafka-${var.environment}"
  location            = var.location
  resource_group_name = var.resource_group_name
  cluster_version     = "4.0"
  tier                = "Standard"

  component_version {
    kafka = "2.1"
  }

  gateway {
    enabled  = true
    username = var.admin_username
    password = var.admin_password
  }

  # storage_account_gen2 {
  #   storage_resource_id = module.storage_kafka.storage_account_id
  #   filesystem_id = azurerm_storage_container.kafka_main.id
  #   managed_identity_resource_id = 
  #   is_default           = true
  # }

  storage_account {
    storage_container_id = azurerm_storage_container.kafka_main[0].id
    storage_account_key  = module.storage_kafka[0].storage_account_access_key
    is_default           = true
  }

  roles {
    head_node {
      vm_size  = var.agents_size
      username = var.admin_username
      password = var.admin_password
      subnet_id = var.subnet_id
      virtual_network_id = var.virtual_network_id
    }
    
    worker_node {
      vm_size                  = var.agents_size
      username                 = var.admin_username
      password                 = var.admin_password
      number_of_disks_per_node = 3
      target_instance_count    = var.instance_count
      subnet_id = var.subnet_id
      virtual_network_id = var.virtual_network_id
    }

    zookeeper_node {
      vm_size  = var.agents_size
      username = var.admin_username
      password = var.admin_password
      subnet_id = var.subnet_id
      virtual_network_id = var.virtual_network_id
    }
  }

  tags = var.tags

}
