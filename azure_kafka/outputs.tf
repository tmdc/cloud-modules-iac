
output "storage_account_id" {
  value = concat(module.storage_kafka.*.storage_account_id, [""])[0]
}

output "storage_account_name" {
  value = concat(module.storage_kafka.*.storage_account_name, [""])[0]
}

output "storage_account_access_key" {
  value = concat(module.storage_kafka.*.storage_account_access_key, [""])[0]
  sensitive   = true
}

output "storage_account_connection_string" {
  value = concat(module.storage_kafka.*.storage_account_connection_string, [""])[0]
  sensitive   = true
}

output "kafka_https_endpoint" {
  value = concat(azurerm_hdinsight_kafka_cluster.main.*.https_endpoint, [""])[0]
}

output "kafka_ssh_endpoint" {
  value = concat(azurerm_hdinsight_kafka_cluster.main.*.ssh_endpoint, [""])[0]
}

output "kafka_id" {
  value = concat(azurerm_hdinsight_kafka_cluster.main.*.id, [""])[0]
}