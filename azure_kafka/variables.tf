variable "prefix" {
  description = "The prefix for the resources created in the specified Azure Resource Group"
}

variable "short_prefix" {
  description = "The short prefix for resources that have name length limitations and delimiter limitations"
}

variable "short_postfix" {
  description = "The short postfix for resources that have name length limitations and delimiter limitations"
  default     = ""
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "location" {
  default     = "westus2"
  description = "The location for the resource"
}

variable "resource_group_name" {
  description = "The name of the resource group that resource should be created in"
}

variable "admin_username" {
  description = "The username of the local administrator to be created on the cluster"
}

variable "admin_password" {
  description = "The password of the local administrator to be created on the cluster"
}

variable "disks_per_node" {
  default = 3
  description = ""
}

variable "instance_count" {
  default = 3
  description = ""
}

variable "subnet_id" {
  description = "Subnet to run Kafka in"
}

variable "virtual_network_id" {
  description = "Virtual network to run Kafka in"
}

variable "agents_size" {
  description = "The Azure VM Size of the Virtual Machines used in the Agent Pool"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}

variable "create_kafka" {
  default = true
  type    = bool
}