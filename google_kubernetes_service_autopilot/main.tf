module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-autopilot-public-cluster"
  project_id                 = var.project_id
  name                       = "${var.prefix}-gke-${var.pet_name}-${var.environment}"
  region                     = var.location
  zones                      = var.availability_zones
  network                    = var.vpc_name
  subnetwork                 = var.subnet_name
  ip_range_pods              = var.subnet_pods_name
  ip_range_services          = var.subnet_services_name
  http_load_balancing        = var.http_application_routing_enabled
  horizontal_pod_autoscaling = true
  kubernetes_version         = var.kubernetes_version
  create_service_account     = true
  deletion_protection        = var.deletion_protection

  cluster_resource_labels = var.tags
}