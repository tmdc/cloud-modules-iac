variable "project_id" {
  description = "The id of the project that the bucket should be created in"
}

variable "location" {
  description = "The location for the resource"
}

variable "prefix" {
  description = "The prefix for the resources created in the specified Azure Resource Group."
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}

variable "availability_zones" {
  type        = list(string)
  description = "The availability zones to run the nodes in."
}

variable "kubernetes_version" {
  description = "Version of Kubernetes to install"
}

variable "http_application_routing_enabled" {
  description = "Enable HTTP Application Routing, this should only be on in Dev environments"
}

variable "vpc_name" {
  description = "The VPC that the Kubernetes service should be a part of"
}

variable "subnet_name" {
  description = "The subnet to host the cluster"
}

variable "subnet_pods_name" {
  description = "The name of the secondary subnet ip range to use for pods"
}

variable "subnet_services_name" {
  description = "The name of the secondary subnet range to use for services"
}

variable "pet_name" {
  description = "petname"
  default     = "owl"
}

variable "deletion_protection" {
  description = "Add delete protection"
  default     = false
}
