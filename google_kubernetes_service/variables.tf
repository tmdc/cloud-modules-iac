variable "project_id" {
  description = "The id of the project that the bucket should be created in"
}

variable "location" {
  description = "The location for the resource"
}

variable "prefix" {
  description = "The prefix for the resources created in the specified Azure Resource Group."
}

variable "short_prefix" {
  description = "The short prefix for resources that have name length limitations and delimiter limitations"
}

variable "short_postfix" {
  description = "The short postfix for resources that have name length limitations and delimiter limitations"
  default     = ""
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}

variable "availability_zones" {
  type        = list(string)
  description = "The availability zones to run the nodes in."
}

variable "min_count" {
  description = "The min number of nodes that should exist in the Pool"
}

variable "max_count" {
  description = "The max number of nodes that should exist in the Pool"
}

variable "node_count" {
  description = "The current number of nodes that should exist in the Pool"
}

variable "agents_size" {
  description = "The Azure VM Size of the Virtual Machines used in the Agent Pool"
}

variable "kubernetes_version" {
  description = "Version of Kubernetes to install"
}

variable "http_application_routing_enabled" {
  description = "Enable HTTP Application Routing, this should only be on in Dev environments"
}


variable "vpc_name" {
  description = "The VPC that the Kubernetes service should be a part of"
}

variable "subnet_name" {
  description = "The subnet to host the cluster"
}

variable "subnet_pods_name" {
  description = "The name of the secondary subnet ip range to use for pods"
}

variable "subnet_services_name" {
  description = "The name of the secondary subnet range to use for services"
}

variable "auto_repair" {
  description = "Auto repair nodes in the default node pool"
  default     = true
}

variable "pet_name" {
  description = "petname"
  default     = "owl"
}

variable "node_pool_unique_string" {
  description = "unique string"
}

variable "disk_size" {
  default = 10
}

variable "enable_filestore_csi_driver" {
  default = false
}

variable "deletion_protection" {
  description = "Add delete protection"
  default = false
}
