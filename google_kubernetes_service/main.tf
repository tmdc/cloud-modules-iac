module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  version                    = "30.2.0" 
  project_id                 = var.project_id
  name                       = "${var.prefix}-gke-${var.pet_name}-${var.environment}"
  region                     = var.location
  zones                      = var.availability_zones
  network                    = var.vpc_name
  subnetwork                 = var.subnet_name
  ip_range_pods              = var.subnet_pods_name
  ip_range_services          = var.subnet_services_name
  http_load_balancing        = var.http_application_routing_enabled
  horizontal_pod_autoscaling = true
  network_policy             = true
  kubernetes_version         = var.kubernetes_version
  remove_default_node_pool   = true
  create_service_account     = true
  default_max_pods_per_node  = "110"
  network_policy_provider    = "CALICO"
  filestore_csi_driver       = var.enable_filestore_csi_driver
  deletion_protection        = var.deletion_protection

  # TODO: High - Decide on these Configurations
  # enable_resource_consumption_export = false

  logging_service    = "none"
  monitoring_service = "none"

  node_pools = [
    {
      name            = "${var.short_prefix}0np0${var.node_pool_unique_string}0${var.environment}"
      machine_type    = var.agents_size
      min_count       = var.min_count
      max_count       = var.max_count
      local_ssd_count = 0
      disk_size_gb    = var.disk_size
      disk_type       = "pd-ssd"
      image_type      = "COS_CONTAINERD"
      auto_repair     = var.auto_repair
      auto_upgrade    = true
      # service_account    = var.service_account
      preemptible        = false
      initial_node_count = var.node_count
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    "${var.short_prefix}0np0${var.environment}" = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {
      "dataos.io/purpose" = "core-kernel"
      "dataos.io/purpose" = "dataos"
    }

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_metadata = {
    all = {}

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_tags = {
    all = []

    "${var.short_prefix}0np0${var.environment}" = [
      "${var.short_prefix}0np0${var.environment}",
    ]
  }

  cluster_resource_labels = var.tags
}