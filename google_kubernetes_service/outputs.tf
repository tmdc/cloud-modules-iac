output "cluster_id" {
  value = module.gke.name
}

output "cluster_ca_certificate" {
  value     = module.gke.ca_certificate
  sensitive = true
}

output "host" {
  value     = module.gke.endpoint
  sensitive = true
}

output "service_account" {
  value = module.gke.service_account
}
