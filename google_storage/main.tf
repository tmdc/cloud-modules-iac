
module "bucket" {
  source  = "terraform-google-modules/cloud-storage/google//modules/simple_bucket"
  version = "5.0.0"
  name = var.name
  project_id  = var.project_id
  location = var.location
  storage_class = var.storage_class
  labels = var.tags
  force_destroy = "true"
}