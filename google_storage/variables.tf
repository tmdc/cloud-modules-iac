variable "project_id" {
  description = "The id of the project that the bucket should be created in"
}

variable "location" {
  description = "The location for the resource"
}

variable "name" {
  description = "The storage name"
}

variable "storage_class" {
  description = "The storage class of the Bucket"
  default = "STANDARD"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}
