
resource "aws_kms_key" "objects" {
  description             = "${var.name} bucket KMS key is used to encrypt objects"
  deletion_window_in_days = 7
  enable_key_rotation     = true
}

resource "aws_kms_alias" "alias" {
  name          = "alias/${var.name}-s3-enc-key"
  target_key_id = aws_kms_key.objects.key_id
}

module "s3_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  bucket  = var.name
  version = "4.1.0"
  acl     = var.acl

  block_public_policy      = var.block_public_policy
  block_public_acls        = var.block_public_acls
  restrict_public_buckets  = var.restrict_public_buckets
  ignore_public_acls       = var.ignore_public_acls
  force_destroy            = true
  control_object_ownership = true
  object_ownership         = "ObjectWriter"

  versioning = {
    enabled = var.enable_versioning
  }

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = aws_kms_key.objects.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  tags = merge(tomap({ "name" = "var.name", }), var.tags)

}
