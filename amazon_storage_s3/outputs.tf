output "s3_bucket_arn" {
  value = module.s3_bucket.s3_bucket_arn
}

output "name" {
  value = module.s3_bucket.s3_bucket_id
}

output "s3_bucket_website_endpoint" {
  value = module.s3_bucket.s3_bucket_website_endpoint
}

output "s3_kms_key_arn" {
  value = aws_kms_key.objects.arn
}
