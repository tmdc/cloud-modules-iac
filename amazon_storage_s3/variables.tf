variable "name" {
}

variable "acl" {
  default = "private"
}

variable "enable_versioning" {
  default = true
}

variable "block_public_policy" {
  default = true
}

variable "block_public_acls" {
  default = true
}

variable "ignore_public_acls" {
  default = true
}

variable "restrict_public_buckets" {
  default = true
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}
