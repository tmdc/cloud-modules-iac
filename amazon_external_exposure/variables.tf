variable "domain_name" {
}

variable "subject_alternative_names" {
  type = list(string)
}

variable "hosted_zone" {
}

variable "ttl" {
  default = 60
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}