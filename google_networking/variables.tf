variable "project_id" {
  description = "The id of the project that the bucket should be created in"
}

variable "location" {
  description = "The location for the resource"
}

variable "short_prefix" {
  description = "The short prefix for resources that have name length limitations and delimiter limitations"
}

variable "short_postfix" {
  description = "The short postfix for resources that have name length limitations and delimiter limitations"
  default     = ""
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}

variable "private_subnets" {
}
variable "private_secondary_subnet01" {
}
variable "private_secondary_subnet02" {
}

variable "public_subnets" {
}

variable "pet_name" {
  description = "petname"
  default = "owl"
}

variable "subnet_unique_string" {
  description = "unique string"
}
