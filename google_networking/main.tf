
resource "null_resource" "dependency_getter" {
  provisioner "local-exec" {
    command = "echo ${length(var.dependencies)}"
  }
}
locals {
  private_sn_key = "${var.location}/${var.short_prefix}-sn-private-${var.subnet_unique_string}-${var.environment}"
  public_sn_key = "${var.location}/${var.short_prefix}-sn-public-${var.subnet_unique_string}-${var.environment}"
  
  private_sn_name = "${var.short_prefix}-sn-private-${var.subnet_unique_string}-${var.environment}"
  private_sn_s01_name = "${var.short_prefix}-sn-private-s01-${var.subnet_unique_string}-${var.environment}"
  private_sn_s02_name = "${var.short_prefix}-sn-private-s02-${var.subnet_unique_string}-${var.environment}"
  public_sn_name = "${var.short_prefix}-sn-public-${var.subnet_unique_string}-${var.environment}"
}

module "cloud_kernel_vpc" {
  source       = "terraform-google-modules/network/google"
  version      = "9.0.0"
  project_id   = var.project_id
  network_name = "${var.short_prefix}-vpc-${var.pet_name}-${var.environment}"

  subnets = [
    {
      subnet_name   = local.public_sn_name
      subnet_ip     = var.public_subnets
      subnet_region = var.location
      subnet_private_access = false
    },
    {
      subnet_name   = local.private_sn_name
      subnet_ip     = var.private_subnets
      subnet_region = var.location
      subnet_private_access = false
    },
  ]
  secondary_ranges = {
   (local.private_sn_name) = [
        {
            range_name    = local.private_sn_s01_name
            ip_cidr_range = var.private_secondary_subnet01
        },
        {
            range_name    = local.private_sn_s02_name
            ip_cidr_range = var.private_secondary_subnet02
        },
    ]
  }
}

variable "dependencies" {
  type = list
}

resource "null_resource" "dependency_setter" {
  depends_on = [
  ]
}

output "depended_on" {
  value = null_resource.dependency_setter.id
}

