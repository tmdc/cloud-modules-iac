output "google_virtual_network" {
  value = module.cloud_kernel_vpc.network_name
}

output "vpc_name" {
  value = module.cloud_kernel_vpc.network.network.name
}

output "vpc_network_self_link" {
  value = module.cloud_kernel_vpc.network_self_link
}

output "subnets" {
  value = module.cloud_kernel_vpc.subnets
}

output "ck_public_subnet_name" {
  value = module.cloud_kernel_vpc.subnets[local.public_sn_key].name
}

output "ck_private_subnet_name" {
  value = module.cloud_kernel_vpc.subnets[local.private_sn_key].name
}

output "ck_private_subnet_secondary01_name" {
  value = local.private_sn_s01_name
}

output "ck_private_subnet_secondary02_name" {
  value = local.private_sn_s02_name
}
