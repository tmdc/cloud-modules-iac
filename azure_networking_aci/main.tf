
resource "azurerm_network_security_group" "public" {
  name                = "${var.short_prefix}-public-nsg-${var.pet_name}-${var.environment}"
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}

resource "azurerm_network_security_group" "private" {
  name                = "${var.short_prefix}-private-nsg-${var.pet_name}-${var.environment}"
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}

resource "azurerm_network_security_group" "aci" {
  name                = "${var.short_prefix}-aci-nsg-${var.pet_name}-${var.environment}"
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = var.tags
}

resource "azurerm_virtual_network" "cloud_kernel_vnet" {
  name                = "${var.short_prefix}-vn-${var.pet_name}-${var.environment}"
  location            = var.location
  resource_group_name = var.resource_group_name
  address_space       = [var.cidr_block]
  tags                = var.tags
}

resource "azurerm_subnet" "public" {
  name                 = "${var.short_prefix}-sn-public-${var.pet_name}-${var.environment}"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.cloud_kernel_vnet.name
  address_prefixes     = [var.public_subnets]
}

resource "azurerm_subnet_network_security_group_association" "public" {
  subnet_id                 = azurerm_subnet.public.id
  network_security_group_id = azurerm_network_security_group.public.id
}

resource "azurerm_subnet" "private" {
  name                 = "${var.short_prefix}-sn-private-${var.pet_name}-${var.environment}"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.cloud_kernel_vnet.name
  address_prefixes     = [var.private_subnets]
  service_endpoints    = ["Microsoft.Storage", "Microsoft.Sql"]
}

resource "azurerm_subnet_network_security_group_association" "private" {
  subnet_id                 = azurerm_subnet.private.id
  network_security_group_id = azurerm_network_security_group.private.id
}

resource "azurerm_subnet" "aci" {
  name                 = "${var.short_prefix}-sn-aci-${var.pet_name}-${var.environment}"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.cloud_kernel_vnet.name
  address_prefixes     = [var.aci_subnets]

  delegation {
    name = "${var.short_prefix}-aci-${var.pet_name}-${var.environment}-delegation"
    service_delegation {
      name    = "Microsoft.ContainerInstance/containerGroups"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}

resource "azurerm_subnet_network_security_group_association" "aci" {
  subnet_id                 = azurerm_subnet.aci.id
  network_security_group_id = azurerm_network_security_group.aci.id
}

resource "azurerm_public_ip" "natpublic_ip" {
  name                = "${var.short_prefix}-${var.environment}-nat-ip"
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
  ip_version          = "IPv4"
  sku                 = "Standard"
}

resource "azurerm_nat_gateway" "gw_aks" {
  name                    = "${var.short_prefix}-nat-${var.environment}-nat-gateway"
  resource_group_name     = var.resource_group_name
  location                = var.location
  sku_name                = "Standard"
  idle_timeout_in_minutes = 10
  zones                   = azurerm_public_ip.natpublic_ip.zones
}

resource "azurerm_nat_gateway_public_ip_association" "nat_pubip_associate" {
  nat_gateway_id       = azurerm_nat_gateway.gw_aks.id
  public_ip_address_id = azurerm_public_ip.natpublic_ip.id
}

resource "azurerm_subnet_nat_gateway_association" "nat_subnet_associate" {
  subnet_id      = azurerm_subnet.private.id
  nat_gateway_id = azurerm_nat_gateway.gw_aks.id
}

resource "azurerm_subnet_nat_gateway_association" "nat_subnet_associate_aci" {
  subnet_id      = azurerm_subnet.aci.id
  nat_gateway_id = azurerm_nat_gateway.gw_aks.id
}
