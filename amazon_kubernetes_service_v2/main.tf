module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.5.1"

  cluster_name    = var.cluster_name
  subnet_ids      = var.subnet_ids
  cluster_version = var.eks_version

  tags = merge(tomap({ "name" = "var.cluster_name", }), var.tags)

  vpc_id = var.vpc_id

  create_cluster_security_group = true
  # cluster_security_group_id = var.security_group_ids

  cluster_endpoint_private_access      = var.enable_private_access
  cluster_endpoint_public_access       = var.enable_public_access
  cluster_endpoint_public_access_cidrs = var.public_cidrs

  cluster_enabled_log_types              = var.eks_log_types
  cloudwatch_log_group_retention_in_days = 7

  eks_managed_node_group_defaults = {
    ami_type       = var.ami_type
    disk_size      = var.disk_size
    subnets        = var.subnet_ids
    instance_types = [var.instance_type]
    # tag_specifications = {
    #   resource_type = "instance"
    #   tags = {
    #     Name = "eks-node-group-instance-name"
    #   }
    # }
  }

  eks_managed_node_groups = {
    core = {
      name         = var.node_group_name
      desired_size = var.desired_size
      max_size     = var.max_size
      min_size     = var.min_size
      disk_size    = var.disk_size

      instance_types  = [var.instance_type]
      k8s_labels      = var.tags
      additional_tags = merge(tomap({ "name" = "var.node_group_name", }))
    }
  }

  # map_roles    = var.map_roles
  # map_users    = var.map_users
  # map_accounts = var.map_accounts

}