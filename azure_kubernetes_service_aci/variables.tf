variable "prefix" {
  description = "The prefix for the resources created in the specified Azure Resource Group."
}

variable "short_prefix" {
  description = "The short prefix for resources that have name length limitations and delimiter limitations"
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "resource_group_name" {
  description = "The name of the Resource Group in which the Kubernetes Will Create"
}

variable "location" {
  description = "The Azure Region in which to create the Virtual Network"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}

variable "admin_username" {
  description = "The username of the local administrator to be created on the Kubernetes cluster"
}

variable "admin_public_ssh_key" {
  description = "The SSH key to be used for the username defined in the `admin_username` variable."
}

variable "min_count" {
  description = "The min number of nodes that should exist in the Pool"
}

variable "max_count" {
  description = "The max number of nodes that should exist in the Pool"
}

variable "node_count" {
  description = "The current number of nodes that should exist in the Pool"
}

variable "agents_size" {
  description = "The Azure VM Size of the Virtual Machines used in the Agent Pool"
}

variable "kubernetes_version" {
  description = "Version of Kubernetes to install"
}

variable "service_cidr" {
  description = "The cidr range for services"
}

variable "dns_service_ip" {
  description = "The ip address of the dns service in AKS"
}

variable "http_application_routing_enabled" {
  description = "Enable HTTP Application Routing, this should only be on in Dev environments"
}

variable "availability_zones" {
  type        = list(string)
  description = "The availability zones to run the nodes in."
}

variable "log_analytics_enabled" {
  description = "The Log Analytics Workspace Id."
  default     = false
}

variable "os_disk_size_gb" {
  default = 50
}

variable "enable_host_encryption" {
  default = false
}

variable "nodes_vnet_subnet_id" {
}

variable "network_policy" {
  default = "azure"
}

variable "network_plugin" {
  default = "azure"
}

variable "pet_name" {
  description = "petname"
  default     = "bob"
}

variable "blob_driver_enabled" {
  default = true
}

variable "disk_driver_version" {
  default = "v1"
}

variable "disk_driver_enabled" {
  default = true
}

variable "file_driver_enabled" {
  default = true
}

variable "sku_tier" {
  default = "Standard"
}

variable "fips_enabled" {
  default = false
}

variable "aci_subnet_name" {
}

variable "aci_subnet_id" {
}

