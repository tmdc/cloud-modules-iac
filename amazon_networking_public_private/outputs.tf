# VPC
output "vpc_id_vppdi" {
  description = "The ID of the VPC"
  value       = module.vpc_public_private.vpc_id
}
# Subnets
output "private_subnets_vppdi" {
  description = "List of IDs of private subnets"
  value       = module.vpc_public_private.private_subnets
}

output "public_subnets_vppdi" {
  description = "List of IDs of public subnets"
  value       = module.vpc_public_private.public_subnets
}

# NAT gateways
output "nat_public_ips_vppdi" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = module.vpc_public_private.nat_public_ips
}

# NAT gateways
output "nat_public_ids_vppdi" {
  description = "List of Allocation IDs of Elastic IPs"
  value       = module.vpc_public_private.nat_ids
}

# intra_route_table
output "intra_route_table_ids_vppdi" {
  description = "List of intra route table IDs"
  value       = module.vpc_public_private.intra_route_table_ids
}

# private_route_table
output "private_route_table_ids_vppdi" {
  description = "List of private route table IDs"
  value       = module.vpc_public_private.private_route_table_ids
}