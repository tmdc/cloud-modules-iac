

module "vpc_public_private" {
  source     = "terraform-aws-modules/vpc/aws"
  version    = "5.7.0"
  create_vpc = var.create_public_private
  name       = var.network_name

  cidr = var.cidr_block

  azs             = var.azs
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets

  create_database_subnet_group = false

  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  # Set the Default NACLS to Deny all inbound and outbound
  manage_default_network_acl  = true
  default_network_acl_ingress = var.network_acls["default_nacl_inbound"]
  default_network_acl_egress  = var.network_acls["default_nacl_outbound"]

  public_dedicated_network_acl = true
  public_inbound_acl_rules = concat(
    var.network_acls["public_inbound"],
  )
  public_outbound_acl_rules = concat(
    var.network_acls["public_outbound"],
  )

  private_dedicated_network_acl = true
  private_inbound_acl_rules = concat(
    var.network_acls["private_inbound"],
  )
  private_outbound_acl_rules = concat(
    var.network_acls["private_outbound"],
  )

  enable_nat_gateway     = var.enable_nat_gateway
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = var.one_nat_gateway_per_az

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                                 = var.enable_flow_log
  create_flow_log_cloudwatch_log_group            = var.enable_flow_log
  create_flow_log_cloudwatch_iam_role             = var.enable_flow_log
  flow_log_cloudwatch_log_group_retention_in_days = 7
  flow_log_cloudwatch_log_group_name_prefix       = var.flow_logs_destination_prefix

  tags = var.tags

  public_subnet_tags  = var.public_subnet_additional_tags
  private_subnet_tags = var.private_subnet_additional_tags

}
