

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"

  cluster_name    = var.cluster_name
  subnets         = var.subnet_ids
  cluster_version = var.eks_version

  write_kubeconfig = false

  tags = merge(tomap({ "name" = "var.cluster_name", }), var.tags)

  vpc_id = var.vpc_id

  cluster_create_security_group = true
  # cluster_security_group_id = var.security_group_ids

  cluster_endpoint_private_access      = var.enable_private_access
  cluster_endpoint_public_access       = var.enable_public_access
  cluster_endpoint_public_access_cidrs = var.public_cidrs

  cluster_enabled_log_types     = var.eks_log_types
  cluster_log_retention_in_days = 7

  node_groups_defaults = {
    ami_type       = var.ami_type
    disk_size      = var.disk_size
    subnets        = var.subnet_ids
    instance_types = [var.instance_type]
    tag_specifications = {
      resource_type = "instance"
      tags = {
        Name = "eks-node-group-instance-name"
      }
    }
  }

  node_groups = {
    core = {
      name             = var.node_group_name
      desired_capacity = var.desired_size
      max_capacity     = var.max_size
      min_capacity     = var.min_size
      disk_size        = var.disk_size

      instance_types  = [var.instance_type]
      k8s_labels      = var.tags
      additional_tags = merge(tomap({ "name" = "var.node_group_name", }))
    }
  }

  # map_roles    = var.map_roles
  # map_users    = var.map_users
  # map_accounts = var.map_accounts

}