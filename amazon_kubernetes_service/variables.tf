variable "vpc_id" {}

variable "cluster_name" {}

variable "subnet_ids" {}

# variable "security_group_ids" {}

variable "eks_version" {
  default = ""
}

variable "enable_private_access" {
  default = true
}

variable "private_cidrs" {
  type = list(any)
  default = [
    "0.0.0.0/0"
  ]
}

variable "enable_public_access" {
  default = false
}

variable "public_cidrs" {
  type = list(any)
  default = [
    "0.0.0.0/0"
  ]
}

variable "eks_log_types" {
  type    = list(any)
  default = []
}

variable "node_group_name" {}

variable "desired_size" {
  default = 2
}

variable "max_size" {
  default = 2
}

variable "min_size" {
  default = 2
}

variable "instance_type" {
  default = "t3.medium"
}

variable "disk_size" {
}

variable "ami_type" {
  default = "AL2_x86_64"
}

variable "public_ssh_key" {
  description = "A custom ssh key to control access to the nodes"
  default     = ""
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}
