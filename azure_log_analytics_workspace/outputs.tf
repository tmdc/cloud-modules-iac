output "id" {
  value = azurerm_log_analytics_workspace.main.id
}

output "name" {
  value = azurerm_log_analytics_workspace.main.name
}

output "random_string" {
  value = random_string.log_analytics_random.result
}

