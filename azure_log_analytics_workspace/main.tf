
resource "random_string" "log_analytics_random" {
  length    = 5
  lower     = true
  numeric   = false
  min_lower = 5
}

resource "azurerm_log_analytics_workspace" "main" {
  name                = "${var.prefix}-law-${random_string.log_analytics_random.result}-${var.environment}"
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = var.sku
  retention_in_days   = var.retention_in_days
  tags                = var.tags
}

