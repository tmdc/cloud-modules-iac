# Output for the SES email identity
output "ses_email_identity" {
  value = aws_ses_email_identity.email_sender.email
}

output "smtp_port" {
  value = 587
}

# Output SMTP Username (IAM Access Key ID)
output "smtp_username" {
  value = aws_iam_access_key.smtp_access_key.id
}

# Output SMTP Password (Secret Access Key as SMTP Password for SES)
output "smtp_password" {
  value     = aws_iam_access_key.smtp_access_key.ses_smtp_password_v4
  sensitive = true
}