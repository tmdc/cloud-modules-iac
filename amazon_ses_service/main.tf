# SES Email Identity
resource "aws_ses_email_identity" "email_sender" {
  email = var.ses_email
}

# IAM User for SES SMTP
resource "aws_iam_user" "ses_smtp_user" {
  name = var.iam_user_name
}

# IAM Access Key for SMTP User
resource "aws_iam_access_key" "smtp_access_key" {
  user = aws_iam_user.ses_smtp_user.name
}

# SES Policy Document (permissions to send email)
data "aws_iam_policy_document" "ses_policy" {
  statement {
    actions   = ["ses:SendEmail", "ses:SendRawEmail"]
    resources = [aws_ses_email_identity.email_sender.arn]
  }
}

# IAM Policy for SES
resource "aws_iam_policy" "ses_policy" {
  name   = var.iam_policy_name
  policy = data.aws_iam_policy_document.ses_policy.json
}

# Attach Policy to IAM User
resource "aws_iam_user_policy_attachment" "ses_policy_attachment" {
  user       = aws_iam_user.ses_smtp_user.name
  policy_arn = aws_iam_policy.ses_policy.arn
}