# Define variables with default values or descriptions

variable "aws_region" {
  description = "The AWS region where SES will be configured"
  type        = string
  default     = "ap-south-1" # Set your preferred default region
}

variable "ses_email" {
  description = "The email address or domain for AWS SES"
}

variable "iam_user_name" {
  description = "The name of the IAM user for SES SMTP access"
  type        = string
  default     = "ses-smtp-user"
}

variable "iam_policy_name" {
  description = "The name of the IAM policy for SES SMTP"
  type        = string
  default     = "ses_smtp_policy"
}

