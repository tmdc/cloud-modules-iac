resource "random_string" "storage_random" {
  count     = var.create_storage ? 1 : 0
  length    = 5
  lower     = true
  numeric   = false
  min_lower = 5
}

resource "azurerm_storage_account" "ck_core_sa" {
  count                    = var.create_storage ? 1 : 0
  name                     = "${var.short_prefix}0${random_string.storage_random[0].result}0${var.short_postfix}0${var.environment}"
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = var.account_tier
  account_kind             = var.account_kind
  access_tier              = "Hot"
  account_replication_type = var.account_replication_type
  is_hns_enabled           = var.is_hns_enabled
  # network_rules {
  #   default_action             = "Deny"
  #   bypass                     = ["Logging", "Metrics", "AzureServices"]
  #   virtual_network_subnet_ids = var.subnet_ids_to_allow
  # }
  tags = var.tags
}