variable "resource_group_name" {
  description = "The name of the resource group that AKS should be created in"
}

variable "location" {
  description = "The location for the resource"
}

variable "account_tier" {
  description = "The account tier of the Storage Account"
  default     = "Standard"
}

variable "account_replication_type" {
  description = "The account replication type of the Storage Account"
  default     = "ZRS"
}

variable "account_kind" {
  description = "The kind of Storage Account"
  default     = "StorageV2"
}

variable "is_hns_enabled" {
  type    = bool
  default = true
}

variable "subnet_ids_to_allow" {
  type        = list(string)
  description = "Subnets to allow access"
}

variable "short_prefix" {
  description = "The short prefix for resources that have name length limitations and delimiter limitations"
}

variable "short_postfix" {
  description = "The short postfix for resources that have name length limitations and delimiter limitations"
  default     = ""
}

variable "environment" {
  description = "The environment for the resources created in the specified Azure Resource Group"
}

variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}

variable "create_storage" {
  default = true
  type    = bool
}
