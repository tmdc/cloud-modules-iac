output "storage_account_id" {
  value = concat(azurerm_storage_account.ck_core_sa.*.id, [""])[0]
}

output "storage_account_name" {
  value = concat(azurerm_storage_account.ck_core_sa.*.name, [""])[0]
}

output "storage_account_access_key" {
  value     = concat(azurerm_storage_account.ck_core_sa.*.primary_access_key, [""])[0]
  sensitive = true
}

output "storage_account_connection_string" {
  value     = concat(azurerm_storage_account.ck_core_sa.*.primary_connection_string, [""])[0]
  sensitive = true
}