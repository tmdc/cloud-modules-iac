data "azurerm_subscription" "current" {
}

resource "azurerm_kubernetes_cluster" "main" {
  name                = "${var.prefix}-aks-${var.pet_name}-${var.environment}"
  location            = var.location
  resource_group_name = var.resource_group_name
  node_resource_group = "${var.resource_group_name}-k8s-nodes"
  dns_prefix          = var.prefix
  kubernetes_version  = var.kubernetes_version
  sku_tier            = var.sku_tier

  linux_profile {
    admin_username = var.admin_username

    ssh_key {
      key_data = replace(var.admin_public_ssh_key, "\n", "")
    }
  }

  storage_profile {
    blob_driver_enabled = var.blob_driver_enabled
    disk_driver_enabled = var.disk_driver_enabled
    disk_driver_version = var.disk_driver_version
    file_driver_enabled = var.file_driver_enabled
  }

  network_profile {
    dns_service_ip    = var.dns_service_ip
    network_plugin    = var.network_plugin
    network_policy    = var.network_policy
    service_cidr      = var.service_cidr
    load_balancer_sku = "standard"
  }

  default_node_pool {
    name            = "${var.short_prefix}0np0${var.environment}"
    vm_size         = var.agents_size
    os_disk_size_gb = var.os_disk_size_gb
    zones           = var.availability_zones
    os_disk_type    = "Ephemeral"
    vnet_subnet_id  = var.nodes_vnet_subnet_id
    min_count       = var.min_count
    max_count       = var.max_count
    node_count      = var.node_count
    node_labels = merge(tomap({
      "dataos.io/layer"   = "system",
      "dataos.io/partOf"  = "dataos",
      "dataos.io/purpose" = "core-kernel",
    }))
    enable_auto_scaling    = true
    enable_host_encryption = var.enable_host_encryption
    type                   = "VirtualMachineScaleSets"
    enable_node_public_ip  = false
    tags                   = var.tags
    max_pods               = 250
    fips_enabled           = var.fips_enabled
  }

  identity {
    type = "SystemAssigned"
  }

  http_application_routing_enabled  = var.http_application_routing_enabled
  role_based_access_control_enabled = true

  depends_on = [var.aks_depends_on]

  tags = var.tags

  lifecycle {
    ignore_changes = [
      tags,
      default_node_pool.0.node_count,
    ]
  }
}

# Assign permissions to the system-assigned managed identity at the resource group level
resource "azurerm_role_assignment" "contributor_main" {
  principal_id         = azurerm_kubernetes_cluster.main.identity[0].principal_id
  role_definition_name = "Contributor"
  scope                = "/subscriptions/${data.azurerm_subscription.current.subscription_id}/resourceGroups/${var.resource_group_name}"

  depends_on = [
    azurerm_kubernetes_cluster.main,
    data.azurerm_subscription.current
  ]
}

# Assign permissions to the system-assigned managed identity at the storage account.
resource "azurerm_role_assignment" "storage_account_main" {
  principal_id   = azurerm_kubernetes_cluster.main.identity[0].principal_id
  role_definition_name = "Storage Account Contributor"
  scope          = "/subscriptions/${data.azurerm_subscription.current.subscription_id}/resourceGroups/${var.resource_group_name}"
  
  depends_on = [
    azurerm_kubernetes_cluster.main,
    data.azurerm_subscription.current
  ]
}

# Assign Storage Account Key Operator Service Role to allow key access
resource "azurerm_role_assignment" "storage_account_key_operator" {
  principal_id         = azurerm_kubernetes_cluster.main.identity[0].principal_id
  role_definition_name = "Storage Account Key Operator Service Role"
  scope                =  "/subscriptions/${data.azurerm_subscription.current.subscription_id}/resourceGroups/${var.resource_group_name}"

  depends_on = [
    azurerm_kubernetes_cluster.main,
    data.azurerm_subscription.current
  ]
}

variable "aks_depends_on" {
  type    = any
  default = null
}
