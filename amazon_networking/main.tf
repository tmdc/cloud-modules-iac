

module "vpc_public_private_database_intra" {
  source = "terraform-aws-modules/vpc/aws"
  create_vpc = var.create_public_private_database_intra
  name = var.network_name

  cidr = var.cidr_block

  azs                 = var.azs
  public_subnets      = var.public_subnets
  private_subnets     = var.private_subnets
  database_subnets    = var.database_subnets
  intra_subnets       = var.intra_subnets
  
  create_database_subnet_group = false
  # create_database_subnet_route_table     = true
  # create_database_internet_gateway_route = true

  enable_dns_hostnames = true
  enable_dns_support   = true

  # Set the Default NACLS to Deny all inbound and outbound
  manage_default_network_acl = true
  default_network_acl_ingress = var.network_acls["default_nacl_inbound"]
  default_network_acl_egress = var.network_acls["default_nacl_outbound"]
  
  public_dedicated_network_acl = true
  public_inbound_acl_rules = concat(
    var.network_acls["default_inbound"],
    var.network_acls["public_inbound"],
  )
  public_outbound_acl_rules = concat(
    var.network_acls["default_outbound"],
    var.network_acls["public_outbound"],
  )

  private_dedicated_network_acl     = true
  private_inbound_acl_rules = concat(
    var.network_acls["default_inbound"],
    var.network_acls["private_inbound"],
  )
  private_outbound_acl_rules = concat(
    var.network_acls["default_outbound"],
    var.network_acls["private_outbound"],
  )

  intra_dedicated_network_acl     = true
  intra_inbound_acl_rules = concat(
    var.network_acls["intra_inbound"],
  )
  intra_outbound_acl_rules = concat(
    var.network_acls["intra_outbound"],
  )

  database_dedicated_network_acl     = true
  database_inbound_acl_rules = concat(
    var.network_acls["database_inbound"],
  )
  database_outbound_acl_rules = concat(
    var.network_acls["default_outbound"],
    var.network_acls["database_outbound"],
  )

  enable_nat_gateway = true
  single_nat_gateway = false
  one_nat_gateway_per_az = true

  # VPC endpoint for S3
  enable_s3_endpoint = true

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_cloudwatch_log_group_retention_in_days = 7
  flow_log_cloudwatch_log_group_name_prefix = var.flow_logs_destination_prefix

  tags = var.tags
  
  public_subnet_tags = var.public_subnet_additional_tags
  private_subnet_tags = var.private_subnet_additional_tags
  
}

module "vpc_public_private" {
  source = "terraform-aws-modules/vpc/aws"
  create_vpc = var.create_public_private
  name = var.network_name

  cidr = var.cidr_block

  azs                 = var.azs
  public_subnets      = var.public_subnets
  private_subnets     = var.private_subnets
  
  create_database_subnet_group = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  # Set the Default NACLS to Deny all inbound and outbound
  manage_default_network_acl = true
  default_network_acl_ingress = var.network_acls["default_nacl_inbound"]
  default_network_acl_egress = var.network_acls["default_nacl_outbound"]
  
  public_dedicated_network_acl = true
  public_inbound_acl_rules = concat(
    var.network_acls["default_inbound"],
    var.network_acls["public_inbound"],
  )
  public_outbound_acl_rules = concat(
    var.network_acls["default_outbound"],
    var.network_acls["public_outbound"],
  )

  private_dedicated_network_acl     = true
  private_inbound_acl_rules = concat(
    var.network_acls["default_inbound"],
    var.network_acls["private_inbound"],
  )
  private_outbound_acl_rules = concat(
    var.network_acls["default_outbound"],
    var.network_acls["private_outbound"],
  )

  enable_nat_gateway = true
  single_nat_gateway = false
  one_nat_gateway_per_az = true

  # VPC endpoint for S3
  enable_s3_endpoint = true

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_cloudwatch_log_group_retention_in_days = 7
  flow_log_cloudwatch_log_group_name_prefix = var.flow_logs_destination_prefix

  tags = var.tags
  
  public_subnet_tags = var.public_subnet_additional_tags
  private_subnet_tags = var.private_subnet_additional_tags

  vpc_endpoint_tags = merge(map(
              "endpoint", "true",
            ), var.tags)
  
}

module "vpc_private_database" {
  source = "terraform-aws-modules/vpc/aws"
  create_vpc = var.create_private_database
  name = var.network_name

  cidr = var.cidr_block

  azs                 = var.azs
  private_subnets     = var.private_subnets
  database_subnets    = var.database_subnets
  
  create_database_subnet_group = false
  # create_database_subnet_route_table     = true
  # create_database_internet_gateway_route = true

  enable_dns_hostnames = true
  enable_dns_support   = true

  # Set the Default NACLS to Deny all inbound and outbound
  manage_default_network_acl = true
  default_network_acl_ingress = var.network_acls["default_nacl_inbound"]
  default_network_acl_egress = var.network_acls["default_nacl_outbound"]

  private_dedicated_network_acl     = true
  private_inbound_acl_rules = concat(
    var.network_acls["private_inbound"],
  )
  private_outbound_acl_rules = concat(
    var.network_acls["default_outbound"],
    var.network_acls["private_outbound"],
  )

  database_dedicated_network_acl     = true
  database_inbound_acl_rules = concat(
    var.network_acls["database_inbound"],
  )
  database_outbound_acl_rules = concat(
    var.network_acls["default_outbound"],
    var.network_acls["database_outbound"],
  )

  enable_nat_gateway = true
  single_nat_gateway = false
  one_nat_gateway_per_az = true

  # VPC endpoint for S3
  enable_s3_endpoint = true

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_cloudwatch_log_group_retention_in_days = 7
  flow_log_cloudwatch_log_group_name_prefix = var.flow_logs_destination_prefix

  tags = var.tags
  
  private_subnet_tags = var.private_subnet_additional_tags
  
  vpc_endpoint_tags = merge(map(
              "endpoint", "true",
            ), var.tags)
}