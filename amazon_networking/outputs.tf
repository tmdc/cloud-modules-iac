# VPC
output "vpc_id_vppdi" {
  description = "The ID of the VPC"
  value       = module.vpc_public_private_database_intra.vpc_id
}

# Subnets
output "private_subnets_vppdi" {
  description = "List of IDs of private subnets"
  value       = module.vpc_public_private_database_intra.private_subnets
}

output "public_subnets_vppdi" {
  description = "List of IDs of public subnets"
  value       = module.vpc_public_private_database_intra.public_subnets
}

output "database_subnets_vppdi" {
  description = "List of IDs of database subnets"
  value       = module.vpc_public_private_database_intra.database_subnets
}

output "intra_subnets_vppdi" {
  description = "List of IDs of intra subnets"
  value       = module.vpc_public_private_database_intra.intra_subnets
}

# NAT gateways
output "nat_public_ips_vppdi" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = module.vpc_public_private_database_intra.nat_public_ips
}
