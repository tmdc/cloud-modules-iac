
variable "create_public_private_database_intra" {
  description = "Create the public_private_database_intra if set to true"
  default = false
}

variable "create_private_database" {
  description = "Create the private_database if set to true"
  default = false
}

variable "create_public_private" {
  description = "Create the public_private if set to true"
  default = false
}

variable "network_name" {}
variable "prefix" {}
variable "environment" {}

variable "cidr_block" {
  default = "10.212.0.0/16"
}

variable "private_subnets" {
  default = []
}

variable "public_subnets" {
  default = []
}

variable "database_subnets" {
  default = []
}

variable "intra_subnets" {
  default = []
}

variable "azs" {
  type = list(string)
}

variable "flow_logs_destination_prefix" {
  default = "cloud-watch-logs"
}

variable "private_subnet_additional_tags" {
}

variable "public_subnet_additional_tags" {
}

variable "network_acls" {
}


variable "tags" {
  default     = {}
  description = "Any tags that should be present on the resources"
  type        = map(string)
}